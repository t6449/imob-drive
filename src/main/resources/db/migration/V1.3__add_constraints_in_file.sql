ALTER TABLE drive.file
    ADD CONSTRAINT PK_file PRIMARY KEY (id),
    ADD CONSTRAINT FK_directory_id_to_directory FOREIGN KEY (directory_id) REFERENCES drive.directory(id)
 ;
