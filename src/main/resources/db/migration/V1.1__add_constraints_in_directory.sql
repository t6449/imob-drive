ALTER TABLE drive.directory
    ADD CONSTRAINT PK_directory PRIMARY KEY (id),
    ADD CONSTRAINT FK_parent_id_to_directory FOREIGN KEY (parent_id) REFERENCES drive.directory(id)
 ;
