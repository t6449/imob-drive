package com.dlabs.imob.drive.core.dtos;

import com.dlabs.imob.drive.core.ImobMapper;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ResponseDto {

    private Object data;

    // Custom Setter for Builder
    public static class ResponseDtoBuilder {

        public ResponseDtoBuilder() {}

        public ResponseDtoBuilder data(Object data, Class dto) {
            this.data = ImobMapper.modelMapper.map(data, dto);
            return this;
        }

        public ResponseDtoBuilder data(Object data) {
            this.data = data;
            return this;
        }
    }

}
