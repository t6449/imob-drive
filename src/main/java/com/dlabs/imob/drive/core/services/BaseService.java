package com.dlabs.imob.drive.core.services;

import com.dlabs.imob.drive.core.models.BaseModel;
import com.dlabs.imob.drive.core.repositories.BaseRepository;

public interface BaseService<
        R extends BaseRepository<E, I>,
        E extends BaseModel<I>,
        I
        > {

    R getRepository();

}
