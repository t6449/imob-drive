package com.dlabs.imob.drive.core.controllers;

import com.dlabs.imob.drive.core.models.BaseModel;
import com.dlabs.imob.drive.core.services.DeletableService;
import com.dlabs.imob.drive.core.repositories.DeletableRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface DeletableController<
        S extends DeletableService<R, E, ID>,
        R extends DeletableRepository<E, ID>,
        E extends BaseModel<ID>,
        ID> {


    S getService();

    @DeleteMapping("/{id}")
    default ResponseEntity<?> deleteById(@PathVariable ID id) {

        this.getService().deleteById(id);

        return ResponseEntity.noContent().build(); // Returns 204

    }

}
