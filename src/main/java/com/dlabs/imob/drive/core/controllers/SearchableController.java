package com.dlabs.imob.drive.core.controllers;

import com.dlabs.imob.drive.core.dtos.ResponseDto;
import com.dlabs.imob.drive.core.models.BaseModel;
import com.dlabs.imob.drive.core.repositories.BaseRepository;
import com.dlabs.imob.drive.core.services.SearchableService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface SearchableController<
        S extends SearchableService<R, E, ID>,
        R extends BaseRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        > {

    S getService();

    @GetMapping("/{id}")
    default ResponseDto findById(@PathVariable ID id) {
        return this.getService().findById(id);
    }

    @GetMapping("")
    default ResponseDto findAll() {
        return this.getService().findAll();
    }

}
