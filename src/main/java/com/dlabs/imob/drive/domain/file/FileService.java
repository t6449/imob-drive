package com.dlabs.imob.drive.domain.file;

import com.dlabs.imob.drive.core.BusinessException;
import com.dlabs.imob.drive.core.dtos.ResponseDto;
import com.dlabs.imob.drive.core.services.DeletableService;
import com.dlabs.imob.drive.domain.directory.DirectoryService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Getter
public class FileService implements DeletableService<FileRepository, FileModel, UUID> {

    @Autowired
    private FileRepository repository;

    @Autowired
    private DirectoryService directoryService;


    public ResponseDto create(MultipartFile file, String directory) {

        FileModel model = null;
        try {
            model = FileModel.builder()
                    .name(StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())))
                    .data(file.getBytes())
                    .type(file.getContentType())
                    .directory(this.directoryService.directoryModelFrom(directory))
                    .build();
        } catch (IOException e) {
            throw new BusinessException("Invalid file.");
        }

        UUID hash = this.repository.save(model).getId();

        return ResponseDto.builder()
                .data(Map.of("hash", hash.toString()))
                .build();
    }

    public ResponseEntity<byte[]> findById(String id) {

        FileModel model = this.repository.findById(UUID.fromString(id))
                .orElseThrow(() -> new BusinessException("File not exist with ID " + id));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "atachment; filename=\"" + model.getName() + "\"")
                .body(model.getData());
    }

    public ResponseDto existenceById(String id) {
        try {
            UUID.fromString(id);
        }
        catch (Exception e) {
           throw new BusinessException("Invalid format id. " + id);
        }
        if (!this.repository.existsById(UUID.fromString(id))) {
            throw new BusinessException("File Id not exist. " + id);
        }
        return ResponseDto.builder()
                .data(Map.of("message", true))
                .build();
    }
}
