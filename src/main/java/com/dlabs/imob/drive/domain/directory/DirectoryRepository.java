package com.dlabs.imob.drive.domain.directory;

import com.dlabs.imob.drive.core.repositories.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DirectoryRepository extends BaseRepository<DirectoryModel, UUID> {
}

