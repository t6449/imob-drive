package com.dlabs.imob.drive.domain.file;

import com.dlabs.imob.drive.core.models.UUIDBaseModel;
import com.dlabs.imob.drive.domain.directory.DirectoryModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "file")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FileModel extends UUIDBaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "data")
    @Lob
    private byte[] data;

    @Column(name = "type")
    private String type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "directory_id")
    private DirectoryModel directory;

}
