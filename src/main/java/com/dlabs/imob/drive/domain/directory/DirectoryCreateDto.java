package com.dlabs.imob.drive.domain.directory;

import com.dlabs.imob.drive.core.dtos.InDto;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class DirectoryCreateDto extends InDto {

    @NotNull(message = "name is required")
    private String name;

    private String parent;

}


