package com.dlabs.imob.drive.domain.file;

import com.dlabs.imob.drive.core.controllers.DeletableController;
import com.dlabs.imob.drive.core.dtos.ResponseDto;
import com.dlabs.imob.drive.shared.Constants;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping(Constants.BASE_URL + "/files")
@Getter
public class FileController implements DeletableController<FileService, FileRepository, FileModel, UUID> {

    @Autowired
    private FileService service;

    @PostMapping(value = "", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseDto create(
            @RequestPart(value = "file", required = true) MultipartFile file,
            @RequestParam(value = "directory", required = false) String directory) {
        return this.service.create(file, directory);
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> findById(@PathVariable String id) {
        return this.service.findById(id);
    }

    @GetMapping("/existence/{id}")
    public ResponseDto existenceById(@PathVariable String id) {
        return this.service.existenceById(id);
    }

}
