package com.dlabs.imob.drive.domain.directory;

import com.dlabs.imob.drive.core.dtos.ResponseDto;
import com.dlabs.imob.drive.core.BusinessException;
import com.dlabs.imob.drive.core.services.BaseService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Getter
public class DirectoryService implements BaseService<DirectoryRepository, DirectoryModel, UUID> {

    @Autowired
    DirectoryRepository repository;

    public ResponseDto create(@Valid DirectoryCreateDto dto) {

        DirectoryModel model = DirectoryModel.builder()
                .name(dto.getName())
                .parent(this.directoryModelFrom(dto.getParent()))
                .build();

        UUID hash = this.repository.save(model).getId();

        return ResponseDto.builder()
                .data(Map.of("hash", hash.toString()))
                .build();
    }

    /**
     * Get directory model from uuid directory
     * @param directory uuid
     * @return directory model
     */
    public DirectoryModel directoryModelFrom(String directory) {

        if (directory == null) return null;

        try {
            UUID id = UUID.fromString(directory);
            return this.repository.findById(id).orElseThrow();
        } catch (Exception ex) {
            throw new BusinessException("Invalid directory id " + directory);
        }


    }

}
