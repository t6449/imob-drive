package com.dlabs.imob.drive.domain.directory;

import com.dlabs.imob.drive.core.dtos.ResponseDto;
import com.dlabs.imob.drive.shared.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.BASE_URL + "/directories")
public class DirectoryController {

    @Autowired
    private DirectoryService service;

    @PostMapping("")
    public ResponseDto create(@RequestBody DirectoryCreateDto dto) {

        return this.service.create(dto);

    }

}
