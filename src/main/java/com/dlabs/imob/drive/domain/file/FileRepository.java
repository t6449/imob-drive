package com.dlabs.imob.drive.domain.file;

import com.dlabs.imob.drive.core.repositories.DeletableRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FileRepository extends DeletableRepository<FileModel, UUID> {
}

